% cada quadro 1m x 1m
% 100 x 100 = 10.000 m2 = 1 ha
% 1.000 x 1.000 = 1.000.000 m2 = 100 ha
    
function linha
    close all; clc; clearvars; clear global; 
    
    maximo=1000;
    n=10;
        
    num_iteracoes=1500;  % baseado no tempo de bateria do dji phantom 4 pro
    deslocamento = 20;   % deslocamento maximo - definido por vel mov de dji phantom 4 pro
    visao = 80;          % campo de visao - def segundo artigos
    dist = 1;            % dist entre os uavs
    xmin = 0;
    xmax = maximo;
    ymin = 0;
    ymax = maximo;
    range = [xmin xmax ymin ymax];
        
    % posiciona
    xn = zeros(1,n);
    yn = zeros(1,n);
    espaco_total = zeros(1,n);

    for i=2:n
        xn(i) = xn(i-1)+dist;
    end
        
    % pos alvo dentro do intervalo
    xt = randi([xmin,xmax]);
    yt = randi([ymin,ymax]);
    
    % exibicao
    figure(1);
    showuav(xn, yn, xt, yt, range);	
 
    achou = zeros(n,1);
    qtdeAchou = 0;
    
    iter_atual = 0;
    direcao=1; %1 subindo
    
    % inicia contagem de tempo
    tic
    
    % repete ate max iteracoes
    while  qtdeAchou<n % iter_atual<=num_iteracoes &&
      iter_atual = iter_atual+1;
      
        xntemp=xn(1);
                    
        andoulado=false;
        %se chegou ao limite superior ou inferior
        if (direcao==1) %subindo
          if (yn(1)==ymax)
              direcao=-1; %muda pra descida
              xn=xn+n;
              andoulado=true;
              mw=n;
          end
        end
        if (direcao==-1) %descendo
          if (yn(1)==ymin)
              direcao=1; %muda pra subida
              xn=xn+n;
              andoulado=true;
              mw=n;
          end
        end 

      qtdeAchou = 0;
      % movimentacao
      for i=1:n
          
        yntemp=yn(i);
          
        %so move se nao achou ainda e nao moveu para o lado (ou para lado
        %ou na vertical)
        if achou(i)==0 & ~andoulado
            %direcao: 1=sobe; -1=desce
            if (direcao==1) %subindo
                if (yn(i)<=ymax-deslocamento) & ~(andoulado)
                    yn(i)=yn(i)+direcao*deslocamento;
                    mw=deslocamento;
                else
                    if yn(i)>ymax-deslocamento
                        mw=abs(ymax-yn(i));
                        yn(i)=ymax;
                    end
                end
            else %descendo
                if (yn(i)>=ymin+deslocamento) & ~(andoulado)
                    yn(i)=yn(i)+direcao*deslocamento;
                    mw=deslocamento;
                else
                    if yn(i)<ymin+deslocamento
                        mw=abs(ymin-yn(i));
                        yn(i)=ymin;
                    end
                end
            end            
        end
        espaco_total(i) = espaco_total(i)+mw;
        movimentacao(iter_atual,i)=mw;
        
        showuav(xn(i), yn(i), xt, yt, range);
        pause(0.001);
        
        dist_alvo = euclideandistance(xn(i), yn(i), xt, yt);
        if (dist_alvo<=visao)
          achou(i) = 1;
          qtdeAchou = qtdeAchou + 1;
        end
      end

      % pra ter animacao
      hold off;
    end
        
    % tempo gasto em segundos
    tempo = toc;

    %beep;
    disp('Varredura')
    disp(' ')
    disp(['Tempo:',num2str(tempo)]);
    disp('Alvo: ');
    disp([num2str(xt),',',num2str(yt)]);
    disp('Espa�o percorrido: ');
    disp(strrep(num2str(espaco_total),'.',','));
    disp('Velocidade: ');
    disp(movimentacao);
        
    fid = fopen('varre.txt', 'a+');
    fprintf(fid, '%s;%s;%s;%s;%s\n', strrep(num2str(tempo),'.',','),num2str(iter_atual),num2str(qtdeAchou),num2str(xt),num2str(yt) );
    fclose(fid);
    
    fid = fopen('varre_dist.txt', 'a+');
    fprintf(fid, '%s\n', strrep(num2str(espaco_total),'.',','));
    fclose(fid);
    
    fid = fopen('varre_veloc.txt', 'a+');
    for ii = 1:size(movimentacao,1)
        movimentacao(ii,:) 
        fprintf(fid,'%s\n',strrep(num2str(movimentacao(ii,:)),'.',','));
    end
    fclose(fid);
    
    disp('---');
    
end

% ------------- funcoes --------------

function showuav(xn, yn, xt, yt, range)
    plot(xn, yn, 'm*');
    hold on;
    plot(xt, yt, 'rO');
    hold on;
    axis(range);
end

function [d]=euclideandistance(x1, y1, x2, y2)
    % dist euclidiana: raiz ( (x2-x1)^2 + (y2-y1)^2 ) 
    d = sqrt((x1-x2)^2+(y1-y2)^2);
end
