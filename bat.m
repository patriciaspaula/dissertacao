% Baseado no trabalho de Xin-She Yang

% cada quadro 1m x 1m
% 100 x 100 = 10.000 m2 = 1 ha
% 1.000 x 1.000 = 1.000.000 m2 = 100 ha ***
    
function tempo=bat
    clc; clear; close all; format short

    global alvo visao
    
    max=350;
    n=10;
    max_iteracoes = 1500;

    maximo = max;
    minimo = 1;
    d=2;            % dimensoes
    desl = 20;      % deslocamento padrao
    margem = 20;

    alvo = [randi([minimo,maximo]) randi([minimo,maximo])];
    visao = 80;
    
    beta=0.5;   % ***** parametro de aprox similar ao usado no pso *****

    A=0.5;      % loudness  0.5
    r=0.5;      % pulse rate  0.5
    passo=0.9;  % afeta atualizacao de A e r
    r0=r;       % guarda valor inicial de r para atualizacao da pos
    Qmin=0;     % frequencia min
    Qmax=1;     % frequencia max 2
    Q=zeros(n,1); % frequencia
    v=zeros(n,d); % velocidade
    
    range = [minimo maximo minimo maximo];
    Sol=zeros(n,2);
    S=zeros(n,2);
    
    espaco_total = zeros(1,n);

    % pos inicial
    posmax=(maximo-minimo)/2+minimo*n;
    posmin=(maximo-minimo)/2-minimo*n;
    for i=1:n
      Sol(i,:)= posmin+(posmax-posmin).*rand(1,d);
      Fitness(i)=objetivo(Sol(i,:));
    end

    % best inicial (minimo)
    [fmin,I]=min(Fitness);
    best=Sol(I,:);

    % inicia contagem de tempo
    tic

    for t=1:max_iteracoes 
        qtdeAchou=0;
        for i=1:n 
            Q(i)= Qmin+(Qmin-Qmax)*rand;

            achou = Fitness<100; % considera best apenas se achou
            if (any(ismember(achou,1)))
                xntemp=S(i,1);
                yntemp=S(i,2);
    
                %adaptado - aproximacao baseada no pso
                pos_tmp=S(i,:);
                %yn      =round(yn.         *(1-b)    + ybest   .*b    +a           .*randi([1,n])-0.5);                
                pos_tmp_x=round(pos_tmp(1,1)*(1-beta) + best(1,1)*beta + abs(Q(i))^i.*randi([1,n]) -0.5);
                pos_tmp_y=round(pos_tmp(1,2)*(1-beta) + best(1,2)*beta + abs(Q(i))^i.*randi([1,n]) -0.5);

                espaco=euclideandistance(xntemp, yntemp, pos_tmp_x, pos_tmp_y);
                
                if espaco>desl
                    perc=(100- desl*100/espaco)/100;
                    difx=abs(xntemp-pos_tmp_x);
                    dify=abs(yntemp-pos_tmp_y);
                                        
                    if pos_tmp_x>xntemp
                        pos_tmp_x=pos_tmp_x-difx*perc;
                    else
                        pos_tmp_x=pos_tmp_x+difx*perc;
                    end
                    if pos_tmp_y>yntemp
                        pos_tmp_y=pos_tmp_y-dify*perc;
                    else
                        pos_tmp_y=pos_tmp_y+dify*perc;
                    end
                    espaco=euclideandistance(xntemp, yntemp, pos_tmp_x, pos_tmp_y);
                end
            
                espaco_total(i) = espaco_total(i)+espaco;
                movimentacao(t,i)=espaco;
                
                S(i,:)= [pos_tmp_x,pos_tmp_y];
                
            else % senao achou, anda aleatoriamente
                vx=desl;
                temp=rand();
                if temp<0.5
                    vx=-1*desl;
                end
                
                vy=desl;
                temp=rand();
                if temp<0.5
                    vy=-1*desl;
                end
                
                espaco_total(i) = espaco_total(i)+desl;
                movimentacao(t,i)=desl;

                v(i,:)= v(i,:)+ [vx,vy];
                S(i,:)=Sol(i,:)+v(i,:);
            end

            % mantem na grid
            S(i,:)=limites(S(i,:),minimo,maximo,margem);

            % atualiza A (loudness) e r (pulse rate)
            if (A>0)
                A=passo*A;
            end
            r=r0*(1-exp(-1*passo*t));
            S(i,:)=S(i,:)+randi([-1,1])*A;

            % atualiza solucao
            Fnew=objetivo(S(i,:)); % testa se achou e atualiza A

            if (Fnew<=Fitness(i))
                if (Fnew<Fitness(i))
                    Sol(i,:)=alvo;
                    qtdeAchou=qtdeAchou+1;
                else
                    Sol(i,:)=S(i,:);
                end
            end
            Fitness(i)=Fnew;

            % atualiza best
            if Fnew<=fmin
                best=S(i,:);
                fmin=Fnew;
            end

            % exibicao
            plot(alvo(1,1),alvo(1,2),'rO');
            temp=Sol(i,:);
            drawnow;
            plot(temp(1,1),temp(1,2),'*');
            hold on;
            axis(range);
            grid on;
            drawnow;
        end

        % exibicao
        pause(0.001);
        hold off;

        % se todos acharam
        achou = Fitness<100;
        if (all(ismember(achou,1)))
            break;
        end
    end

    % tempo gasto em segundos
    tempo = toc;

    %beep;
    disp('BA')
    disp(['Tempo:',num2str(tempo)]);
    disp('Alvo: ');
    disp(alvo);
    disp('Espa�o percorrido: ');
    disp(strrep(num2str(espaco_total),'.',','));
    disp('Velocidade: ');
    disp(movimentacao);
    
    fid = fopen('bat.txt', 'a+');
    fprintf(fid, '%s;%s;%s;%s;%s\n', strrep(num2str(tempo),'.',','),num2str(t),num2str(qtdeAchou),num2str(alvo(1,1)),num2str(alvo(1,2)) );
    fclose(fid);
    
    fid = fopen('bat_dist.txt', 'a+');
    fprintf(fid, '%s\n', strrep(num2str(espaco_total),'.',','));
    fclose(fid);
    
    fid = fopen('bat_veloc.txt', 'a+');
    for ii = 1:size(movimentacao,1)
        movimentacao(ii,:) 
        fprintf(fid,'%s\n',strrep(num2str(movimentacao(ii,:)),'.',','));
    end
    fclose(fid);
    
    disp('---');

end

% ------------- funcoes --------------

function [d]=euclideandistance(x1, y1, x2, y2)
    % dist euclidiana: raiz ( (x2-x1)^2 + (y2-y1)^2 ) 
    d = sqrt((x1-x2)^2+(y1-y2)^2);
end

function t=objetivo(Sol)
    global alvo visao 
    %A    
    t(1,1)=100; %fitness quando nao achou
    
    x1=Sol(1,1);
    y1=Sol(1,2);
    x2=alvo(1,1);
    y2=alvo(1,2);
    dist = sqrt((x1-x2)^2+(y1-y2)^2); % dist euclidiana
    if (dist<=visao)
        t(1,1)=dist; % fitness eh a dist
        %disp('achou');
        %A=0; % se achou, A passa a 0 (silencio)
    end
end

function s=limites(s,minimo,maximo,margem)
  pos_tmp=s;
  margem=randi([margem/2,margem*1.5]);
  
  I1=pos_tmp<minimo;
  while (any(ismember(I1,1)))
    if (pos_tmp(1,1)<=minimo)
        pos_tmp(1,1) = pos_tmp(1,1) + margem;
    end
    if (pos_tmp(1,2)<=minimo)
        pos_tmp(1,2) = pos_tmp(1,2) + margem;
    end
    I1=pos_tmp<minimo;
  end

  I2=pos_tmp>maximo;
  while (any(ismember(I2,1)))
    if (pos_tmp(1,1)>=maximo)
        pos_tmp(1,1) = pos_tmp(1,1) - margem;
    end
    if (pos_tmp(1,2)>=maximo)
        pos_tmp(1,2) = pos_tmp(1,2) - margem;
    end
    I2=pos_tmp>maximo;
  end
  
  s = pos_tmp;
end
