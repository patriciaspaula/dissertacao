% Baseado no trabalho de Kennedy-Ebehnhart e Xin-She Yang

% cada quadro 1m x 1m
% 100 x 100 = 10.000 m2 = 1 ha
% 1.000 x 1.000 = 1.000.000 m2 = 100 ha ***
    
function pso
    close all; clc; clearvars; clear global;  

    maximo=1000;
    n=10;
    max_iteracoes=1500;
    
    xmin = 1;
    xmax = maximo;
    ymin = 1;
    ymax = maximo;
    distxy = 5; % dist entre as particulas
    % para deslocamento maximo ser o 20m/s definido por vel mov de dji phantom 4 pro
    mov = 14;
    quadros = 20;
    visao = 80;
    qtdeAchou = 0; % quantos acharam o alvo
    
    %  eixos da grid
    range = [xmin xmax ymin ymax];
    
    espaco_total = zeros(1,n);
    
    % gera pos inicial das n particulas
    xn = zeros(1,n);
    yn = zeros(1,n);
    
    % correcao quando atingir limite
    margem = 20; %distxy*10;
    
    % posiciona aleatorio no centro da grid
    max=round((xmax-xmin)/2+xmin*n);
    min=round((xmax-xmin)/2-xmin*n);
    for i=1:n
      xn(i)= round(min+(max-min).*rand);
      yn(i)= round(min+(max-min).*rand);
    end
    
    % pos alvo - gera num inteiro no intervalo
    xt = randi([xmin,xmax]);
    yt = randi([ymin,ymax]);
    
    % exibe
    plot(xt,yt,'O');
    drawnow;
    grid on;
    
    % melhores valores - inicia com valor nao usado
    xbest = -10;
    ybest = -10;
    
    % veloc convergencia (0->1)=(lento->rapido)
    beta = 0.5;
    gamma = 0.7;
   
    % inicia contagem de tempo
    tic
    
    i = 1;
    while i<=max_iteracoes && qtdeAchou<n
        plot(xt,yt,'O');
        hold on; 
        
        % tra�a cam de todas particulas
        plot(xn,yn,'*');
        axis(range);
        grid on;
        
        alpha = gamma ^ i;
        
        % move particulas para novo local
        [xn,yn,mw] = pso_move(xn,yn,xbest,ybest,alpha,beta,range,mov,quadros,margem);
        espaco_total = espaco_total+mw;
        movimentacao(i,:)=mw;
            
        qtdeAchou=0;
        for w=1:n
            dist = euclideandistance(xn(w), yn(w), xt, yt);
            
            % se esta no campo de visao
            if (dist<=visao)
                xbest = xt;
                ybest = yt;
                qtdeAchou = qtdeAchou + 1;
            end
        end
        
        drawnow;
        pause(0.001);
        hold off;

        i = i + 1;
    end
    
    % tempo gasto em segundos
    tempo = toc;
    
    %beep;
    disp('PSO')
    disp(['Tempo:',num2str(tempo)]);
    disp('Alvo: ');
    disp([num2str(xt),',',num2str(yt)]);
    disp('Espa�o percorrido: ');
    disp(strrep(num2str(espaco_total),'.',','));
    disp('Velocidade: ');
    disp(movimentacao);

    fid = fopen('pso.txt', 'a+');
    fprintf(fid, '%s;%s;%s;%s;%s\n', strrep(num2str(tempo),'.',','),num2str(i),num2str(qtdeAchou),num2str(xt),num2str(yt) );
    fclose(fid);
    
    fid = fopen('pso_dist.txt', 'a+');
    fprintf(fid, '%s\n', strrep(num2str(espaco_total),'.',','));
    fclose(fid);
    
    fid = fopen('pso_veloc.txt', 'a+');
    for ii = 1:size(movimentacao,1)
        movimentacao(ii,:) 
        fprintf(fid,'%s\n',strrep(num2str(movimentacao(ii,:)),'.',','));
    end
    fclose(fid);
    
    disp('---');
end

% ------------- funcoes --------------

function [d]=euclideandistance(x1, y1, x2, y2)
    % dist euclidiana: raiz ( (x2-x1)^2 + (y2-y1)^2 ) 
    d = sqrt((x1-x2)^2+(y1-y2)^2);
end

function [xn,yn,espaco]=pso_move(xn,yn,xbest,ybest,a,b,range,mov,quadros,margem) 
    num = size(yn,2);
    espaco = zeros(1,num);
    
    m=mov;
    xntemp=xn;
    yntemp=yn;
    
    if (xbest == -10)
        temp=rand();
        if temp<0.5
            m=-1*m;
        end
        vx=m;
        xn = xn + vx;
    else
        xn = round(xn.*(1-b)+xbest.*b+a.*randi([1,num])-0.5);
    end
    
    if (ybest == -10)
        temp=rand();
        if temp<0.5
            m=-1*m;
        end
        vy=m;
        yn = yn + vy;
    else
        yn = round(yn.*(1-b)+ybest.*b+a.*randi([1,num])-0.5);
    end
    
    for i=1:num
        espaco(i)=euclideandistance(xntemp(i), yntemp(i), xn(i), yn(i));
        if espaco(i)>quadros
            perc=(100- quadros*100/espaco(i))/100;
            difx=abs(xntemp(i)-xn(i));
            dify=abs(yntemp(i)-yn(i));

            if xn(i)>xntemp(i)
                xn(i)=xn(i)-difx*perc;
            else
                xn(i)=xn(i)+difx*perc;
            end
            if yn(i)>yntemp(i)
                yn(i)=yn(i)-dify*perc;
            else
                yn(i)=yn(i)+dify*perc;
            end
            espaco(i)=euclideandistance(xntemp(i), yntemp(i), xn(i), yn(i));
        end
        
        while (xn(i) < range(1))||(xn(i) > range(2))
          if (xn(i) < range(1))
              xn(i) = xn(i) + margem;
          end
          if (xn(i) > range(2))
              xn(i) = xn(i) - margem;
          end
        end
        while (yn(i) < range(3))||(yn(i) > range(4))
          if (yn(i) < range(3))
              yn(i) = yn(i) + margem;
          end
          if (yn(i) > range(4))
              yn(i) = yn(i) - margem;
          end
        end
        espaco(i)=euclideandistance(xntemp(i), yntemp(i), xn(i), yn(i));
%         if (espaco(i)>20)
%             xntemp(i)
%             yntemp(i)
%             xn(i)
%             yn(i)
%         end
        
    end
            
    %mantem dentro da grid
%     for i=1:num
%         
%     end

end
